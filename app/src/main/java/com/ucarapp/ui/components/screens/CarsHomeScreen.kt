package com.ucarapp.ui.components.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.ucarapp.data.CarsSingleton
import com.ucarapp.ui.components.car.CarItemView
@Composable
fun CarsHomeScreen (){
    Surface(
        modifier =
        Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ){
        val carList = remember {
            CarsSingleton.getCars()
        }
        LazyColumn{
            items(CarsSingleton.getCars()){ car ->
                CarItemView(car = car)
            }
        }
    }
}
@Preview
@Composable
fun PreviewCarsHomeScreen () {
    CarsHomeScreen()
}