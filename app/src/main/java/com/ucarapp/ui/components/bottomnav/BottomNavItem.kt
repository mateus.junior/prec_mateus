package com.ucarapp.ui.components.bottomnav

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.ucarapp.R

sealed class BottomNavItem(
    val route: String,
    @StringRes val titleRes: Int,
    @DrawableRes val iconRes: Int
) {
    object Home: BottomNavItem("cars", R.string.cars, R.drawable.ic_baseline_directions_car_24)
    object CRUD: BottomNavItem("crud", R.string.crud, R.drawable.ic_baseline_add_box_24)
}
