package com.ucarapp.ui.components.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ucarapp.R
import com.ucarapp.ui.components.bottomnav.BottomNav
import com.ucarapp.ui.components.bottomnav.BottomNavGraph

@Composable
fun MainScreenView() {
    val navController = rememberNavController()
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.app_name),
                        color =
                        if(MaterialTheme.colors.isLight)
                            androidx.compose.ui.graphics.Color.Black
                        else
                            androidx.compose.ui.graphics.Color.White,
                        fontSize = 20.sp
                    )
                },
                backgroundColor = MaterialTheme.colors.primary,
                elevation = 10.dp
            )
        },
        bottomBar = {
            BottomNav(navController = navController)
        }
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            BottomNavGraph(navHostController = navController)
        }
    }
}
