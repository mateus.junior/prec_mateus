package com.ucarapp.ui.components.car

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ucarapp.R
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import com.ucarapp.model.Car
import com.ucarapp.ui.theme.Shapes
import com.ucarapp.ui.theme.UCarAppTheme

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CarItemView(
    car: Car,
    shape: Shape = Shapes.medium
){
    var expandedState by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (expandedState) 180f else 0f
    )
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(150.dp)
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            ),
        shape = shape,
        onClick = {
            expandedState = !expandedState
        }
    ) {
        Column(){
            CarsFirstDetailsColumn(
                car = car,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(vertical = 5.dp)
                    .padding(start = 10.dp)
                )
            CarsSecondDetailsColumn(
                car = car,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(vertical = 5.dp)
                    .padding(start = 10.dp)
            )

            IconButton(
                modifier = Modifier
                    .weight(1f)
                    .alpha(ContentAlpha.medium)
                    .rotate(rotationState),
                onClick = {
                    expandedState = !expandedState
                }) {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = "Drop-Down Arrow"
                )
            }
            if(expandedState)
            {
                CarDescription(
                    car = car,
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                        .padding(vertical = 5.dp)
                        .padding(start = 10.dp)
                )
            }else
            {
                Text(text = "Click to view description")
            }
        }
    }
}
@Composable
fun CarPosterThumb(car: Car, modifier: Modifier = Modifier)
{

    Image(
        painter = painterResource(id = R.drawable.qr700),
        contentDescription = stringResource(id = R.string.poster_image_thumbnail),
        modifier = modifier,
        contentScale = ContentScale.Crop
    )
}
@Composable
fun CarDescription(car: Car,modifier: Modifier = Modifier) {
    Row(modifier = modifier) {
        Text(
            text =  car.description,
            style = MaterialTheme.typography.h5,
            maxLines = 3,
            overflow = TextOverflow.Ellipsis,
            fontFamily = FontFamily.Serif
        )
    }
}
@Composable
fun CarsSecondDetailsColumn(car: Car,modifier: Modifier = Modifier){
    Row(modifier = modifier) {
        Text(
            text = car.category,
            style = MaterialTheme.typography.h5,
            maxLines = 1,
            fontFamily = FontFamily.Serif
        )
        Box(modifier = modifier)
        Text(
            text = "Min Val: R$ " + car.min_value.toString(),
            style = MaterialTheme.typography.h5,
            maxLines = 1,
            fontFamily = FontFamily.Serif
        )
    }
}
@Composable
fun CarsFirstDetailsColumn(car: Car,modifier: Modifier = Modifier){
    Row(modifier = modifier) {
        Text(
            text = "Cod: "+car.id.toString(),
            style = MaterialTheme.typography.h5,
            maxLines = 1,
            fontFamily = FontFamily.Serif
        )
        Box(modifier = modifier)
        Text(
            text = car.entry_date,
            style = MaterialTheme.typography.h5,
            maxLines = 1,
            fontFamily = FontFamily.Serif
        )
    }
}
@Preview
@Composable
fun PreviewCarItemView()
{
 UCarAppTheme(darkTheme = false) {
     val car = Car(0,"SEDAN","Carro vrum vrum","04/03/2001",2000.0)
    CarItemView(car = car)
 }
}