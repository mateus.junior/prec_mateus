package com.ucarapp.ui.components.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ucarapp.data.RequestCar
import com.ucarapp.model.Car
import com.ucarapp.ui.theme.UCarAppTheme

@Composable
fun RegisterCarScreen(
    requestCar: RequestCar
    )
{

    var category = remember { mutableStateOf(TextFieldValue("")) }
    var entrDate = remember { mutableStateOf(TextFieldValue("")) }
    var minValue = remember { mutableStateOf(TextFieldValue("")) }
    var description = remember { mutableStateOf(TextFieldValue("")) }

    Column(modifier =
    Modifier
        .fillMaxWidth()
        .padding(all = 15.dp),
        horizontalAlignment =  Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Text(text = "Castro de Carros")

        TextField(
        modifier = Modifier .padding(top = 25.dp, bottom = 10.dp),
        value = category.value,
        onValueChange = { textFieldValue: TextFieldValue -> category.value =  textFieldValue},
        label ={Text(text = "Category: ")},
        maxLines = 1
        )
        TextField(
            modifier = Modifier .padding(bottom = 10.dp),
            value = entrDate.value,
            onValueChange = { textFieldValue: TextFieldValue -> entrDate.value =  textFieldValue},
            label ={Text(text = "Entry date: ")},
            maxLines = 1,
            placeholder = { Text(text = "Type here...")}
        )
        TextField(
            modifier = Modifier .padding(bottom = 10.dp),
            value = minValue.value,
            onValueChange = { textFieldValue: TextFieldValue -> minValue.value =  textFieldValue},
            label ={Text(text = "Min Value: ")},
            maxLines = 1,
            placeholder = { Text(text = "Type here...")}
        )
        TextField(
            modifier = Modifier .padding(bottom = 10.dp),
            value = description.value,
            onValueChange = { textFieldValue: TextFieldValue -> description.value =  textFieldValue},
            label ={Text(text = "Description: ")},
            maxLines = 4,
            placeholder = { Text(text = "Type here...")}
        )
        Row() {
            Button(onClick = {
                category.value = TextFieldValue("");
                entrDate.value = TextFieldValue("");
                minValue.value = TextFieldValue("");
                description. value = TextFieldValue("");
            },
                modifier = Modifier .padding(all = 5.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Yellow))

            {
                Text(text = "Limpar Campos",color = Color.Black)
            }
            Button(onClick = {
                val newCar: Car = Car(0, category.value.text, description.value.text,entrDate.value.text, minValue.value.text.toDouble());

                //ADICIONAR CARRO
                if(category.value.text == "" || entrDate.value.text==""
                    ||minValue.value.text==""||description.value.text=="")
                {
                    requestCar.addCarAPI(newCar)
                    //LIMPAR CAMPOS
                    category.value = TextFieldValue("")
                    entrDate.value = TextFieldValue("")
                    minValue.value = TextFieldValue("")
                    description. value = TextFieldValue("")
                }

            },
                modifier = Modifier .padding(all = 5.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Magenta))
            {
                Text(text = "Cadastrar",color = Color.White)
            }
        }
    }
}


@Preview
@Composable
fun PreviewRegisterCarScreen(){
    val context = LocalContext.current;
    var requestCar: RequestCar = RequestCar(context);
    UCarAppTheme(darkTheme = false) {
        val car = Car(0,"SEDAN","Carro vrum vrum","04/03/2001",2000.0)
        RegisterCarScreen(requestCar)
    }
}

