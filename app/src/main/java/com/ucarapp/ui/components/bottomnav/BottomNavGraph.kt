package com.ucarapp.ui.components.bottomnav

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController

import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.ucarapp.data.RequestCar
import com.ucarapp.ui.components.screens.CarsHomeScreen
import com.ucarapp.ui.components.screens.RegisterCarScreen

@Composable
fun BottomNavGraph(navHostController: NavHostController) {
    NavHost(navController = navHostController,startDestination = BottomNavItem.Home.route
    ) {

        composable(BottomNavItem.Home.route) {
            CarsHomeScreen()
        }
        composable(BottomNavItem.CRUD.route) {
            val context = LocalContext.current;
            var requestCar: RequestCar = RequestCar(context);
            RegisterCarScreen(requestCar)
        }
    }
}
