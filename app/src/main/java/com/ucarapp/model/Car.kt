package com.ucarapp.model

import java.util.*

class Car (
    val id: Int,
    val category: String,
    val description: String,
    val entry_date: String,
    val min_value: Double
){

}