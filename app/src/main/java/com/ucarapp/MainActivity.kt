package com.ucarapp

import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.toArgb
import com.ucarapp.data.RequestCar
import com.ucarapp.ui.components.screens.MainScreenView
import com.ucarapp.ui.theme.UCarAppTheme


class MainActivity : ComponentActivity() {
    private lateinit var requestCar: RequestCar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestCar = RequestCar(this)
        this.requestCar.startRequestingCars()
        setContent {
            UCarAppTheme() {
                this.SetupConfigsUiConfigs()
                MainScreenView()
            }
        }
    }
    @Composable
    private fun SetupConfigsUiConfigs() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (MaterialTheme.colors.isLight) {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        APPEARANCE_LIGHT_STATUS_BARS, APPEARANCE_LIGHT_STATUS_BARS
                    )
            } else {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        0, APPEARANCE_LIGHT_STATUS_BARS
                    )
            }
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }
}
