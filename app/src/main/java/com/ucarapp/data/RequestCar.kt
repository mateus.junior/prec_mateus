package com.ucarapp.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.ucarapp.model.Car
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class RequestCar(context: Context) {
    private val queue = Volley.newRequestQueue(context)
    companion object{
        private val url = "http://192.168.1.5:5000/"
    }
    fun startRequestingCars()
    {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object :Runnable{
            override fun run() {
                processRequest()
                handler.postDelayed(this,5000)
            }
        })
    }
    private fun processRequest()
    {
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET,
            url,
            null,
            {
                response ->
                    val cars = this.JSONArrayToCars(response)
                    CarsSingleton.updateCars(cars)
            },
            {
                volleyError ->
                Log.e("RequestCarError", "Connection error. ${volleyError.toString()}")
            }
        )
        this.queue.add((jsonArrayRequest))
    }

    fun JSONArrayToCars(jsonArray: JSONArray): ArrayList<Car>{
        val cars = ArrayList<Car>()
        for (i in 0.. (jsonArray.length()-1)){
            val jsonObjt = jsonArray.getJSONObject(i)
            val id = jsonObjt.getInt("id")
            val category = jsonObjt.getString("category")
            val description = jsonObjt.getString("description")
            val entry_date = jsonObjt.getString("date")
            val min_value = jsonObjt.getDouble("min_value")
            cars.add(
                Car(
                    id=id,
                    category,
                    description,
                    entry_date,
                    min_value
                )
            )
        }
        return  cars
    }
    fun addCarAPI(car: Car){
        var body = JSONObject();
        body.put("category",car.category)
        body.put("description",car.description)
        body.put("entry_date",car.entry_date)
        body.put("min_value",car.min_value)

        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.POST,
            "${url}/cars/add",
            body,
            { response -> processRequest()},
            { volleyError ->}
        )
        this.queue.add(jsonArrayRequest);
    }
}