package com.ucarapp.data

import androidx.compose.runtime.mutableStateListOf
import com.ucarapp.model.Car

object CarsSingleton
{
    private val cars = mutableStateListOf<Car>()
    fun updateCars(cars: ArrayList<Car>){
        this.cars.clear()
        this.cars.addAll(cars)
    }
    fun getCars(): List<Car>{
        return this.cars
    }
}